// TODO:
// - channel mixing (compression etc)
// - play with different synthesis algorithms (additive, frequency modulation, physically based, ..)
// - decide on the low-level API
//   should filters & mixers be
//     `params -> lines -> (dt -> value)` or
//     `params -> (inputs -> value)` with function binding?
//   probably the latter, since thread-safe wrappers for the other direction are not free. Also
//   allows e.g. time invariant filters etc to express that fact in their type.
// - effects! (delay, reverb, phaser, etc..)
// - higher level (MIDI-like?) API (state of held notes, ..)
//    scale agnostic?
// - basic console interface (MIDI in, cpal out)

// On synthesis:
// http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.2.5583&rep=rep1&type=pdf
// https://hal.inria.fr/hal-00873639/document
// https://scholarsarchive.byu.edu/cgi/viewcontent.cgi?httpsredir=1&article=2001&context=facpub
// https://pdfs.semanticscholar.org/8c2c/f4eb054e814404697926915ab8e7c8e4d136.pdf

#![feature(existential_type)]

use midir::*;
use cpal::*;
use std::ops;

pub type Value = f64;
pub type Time = f64;
pub type Hz = f64;
pub type Step = f64;

/// Convert a midi semitone to a frequency
pub fn midi_to_freq(s: Step) -> Hz {
   const SEMITONE_INTERVAL: f64 = 1.05946309436; // Twelth root of 2
   const A4_STEP: f64 = 69.0;
   const A4_FREQ: f64 = 440.0;

   A4_FREQ * SEMITONE_INTERVAL.powf(s - A4_STEP)
}

/// The type returned when mapping a CV by some function.
existential type Map<Line, F>: CV;

/// A control voltage.
pub trait CV: Clone {
   /// Get the next value of the line.
   fn sample(&mut self, dt: Time) -> Value;

   /// Apply some function to the CV.
   fn map<F>(mut self, f: F) -> Map<Self, F> where
      F: Fn(Value) -> Value,
      F: Clone
   {
      move |dt| f(self.sample(dt))
   }
}

// Implement CV for closures.
impl<T: FnMut(Time) -> Value> CV for T where T: Clone {
   fn sample(&mut self, dt: Time) -> Value { (self)(dt) }
}

// Implement CV for constants.
impl CV for Value {
   fn sample(&mut self, _dt: Time) -> Value { *self }
}

/// Oscillators
pub mod osc {
   use super::*;

   /// A sine wave oscillator
   pub fn sin(mut freq: impl CV) -> impl CV {
      let mut period = 2.0 * std::f64::consts::PI;
      let mut accum = 0.0;
      move |dt| {
         let freq = freq.sample(dt);
         accum += freq * dt * period;
         if accum > period { accum -= period; }
         f64::sin(accum)
      }
   }

   /// A sawtooth wave oscillator. Rake gives the cosine of the angle between
   /// the horizontal and the start of the wave, set to 1.0 for a standard
   /// sawtooth wave or 0.5 for a triangle wave.
   pub fn saw(mut freq: impl CV, mut rake: impl CV) -> impl CV {
      let mut accum = 0.0;
      move |dt| {
         let freq = freq.sample(dt);
         let rake = rake.sample(dt);
         accum += freq * dt;
         if accum > 1.0 { accum -= 1.0 }
         if accum < rake {
            (accum / rake) * 2.0 - 1.0
         } else {
            (accum - rake) / (1.0 - rake) * 2.0 - 1.0
         }
      }
   }

   /// A triangle wave oscillator
   pub fn triangle(mut freq: impl CV) -> impl CV {
      saw(freq, 0.5)
   }

   /// A square wave oscillator
   pub fn square(mut freq: impl CV) -> impl CV {
      pulse(freq, 0.5)
   }

   /// A pulse wave oscillator
   pub fn pulse(mut freq: impl CV, mut width: impl CV) -> impl CV {
      let mut accum = 0.0;
      move |dt| {
         let freq = freq.sample(dt);
         let width = width.sample(dt);
         accum += freq * dt;
         if accum > 1.0 { accum -= 1.0 }
         if accum < width { 1.0 } else { 0.0 }
      }
   }
}

/// Combining multiple lines into one
pub mod mix {
   use super::*;

   pub fn add(mut l1: impl CV, mut l2: impl CV) -> impl CV {
      move |dt| l1.sample(dt) + l2.sample(dt)
   }

   pub fn sub(mut l1: impl CV, mut l2: impl CV) -> impl CV {
      move |dt| l1.sample(dt) - l2.sample(dt)
   }

   pub fn mul(mut l1: impl CV, mut l2: impl CV) -> impl CV {
      move |dt| l1.sample(dt) * l2.sample(dt)
   }

   pub fn div(mut l1: impl CV, mut l2: impl CV) -> impl CV {
      move |dt| l1.sample(dt) / l2.sample(dt)
   }

   pub fn avr(mut l1: impl CV, mut l2: impl CV) -> impl CV {
      mix::mul(mix::add(l1, l2), 0.5)
   }

   /// Gives the sum of a set of lines
   pub fn sum(iter: impl Iterator<Item=impl CV>) -> impl CV {
      let mut lines = iter.collect::<Vec<_>>();
      move |dt| {
         lines.iter_mut().map(|l| l.sample(dt)).sum()
      }
   }

   /// Given an iterator over lines and weights creates a line giving the weighted sum of
   /// the input lines.
   pub fn weighted_sum(lines: impl Iterator<Item=(impl CV, impl CV)>) -> impl CV {
      let mut lines = lines.collect::<Vec<_>>();
      move |dt| {
         lines.iter_mut().map(|(l, w)| l.sample(dt) * w.sample(dt)).sum()
      }
   }

   /// Linearly interpolate between `min` and `max` based on `value`.
   pub fn lerp(mut min: impl CV, mut max: impl CV, mut value: impl CV) -> impl CV {
      move |dt| {
         let min = min.sample(dt);
         let max = max.sample(dt);
         let value = value.sample(dt);
         min + (value * (max - min))
      }
   }

   /// Inverse of lerp, takes a value in some range and gives back the relative position of
   /// the value in that range.
   pub fn normalize(mut min: impl CV, mut max: impl CV, mut value: impl CV) -> impl CV {
      move |dt| {
         let min = min.sample(dt);
         let max = max.sample(dt);
         let value = value.sample(dt);
         (value - min) / (max - min)
      }
   }
}

/// The parameters of an envelope
#[derive(Clone, Copy, Debug)]
pub struct Envelope {
   delay: Time,
   attack: Time,
   hold: Time,
   decay: Time,
   sustain: Value,
   release: Time
}

impl Envelope {
   pub fn adsr(attack: Time, decay: Time, sustain: Value, release: Value) -> Envelope {
      Envelope { delay: 0.0, attack, hold: 0.0, decay, sustain, release}
   }
}

/// Create a linear envelope with the given parameters.
pub fn linear_envelope(e: Envelope, mut release: impl CV) -> impl CV {
   let mut accum = 0.0;

   let attack_end = e.delay + e.attack;
   let hold_end = attack_end + e.hold;
   let decay_end = hold_end + e.decay;
   let release_end = decay_end + e.release;

   let mut released = false;

   move |dt| {
      released |= release.sample(dt) > 0.5;
      accum += dt;
      if accum < e.delay {
         0.0
      }
      else if accum < attack_end {
         (accum - e.delay) / e.attack
      }
      else if accum < hold_end {
         1.0
      }
      else if accum < decay_end {
         let rel = (accum - hold_end) / e.decay;
         1.0 - rel * (1.0 - e.sustain)
      }
      else if !released {
         accum = decay_end;
         e.sustain
      }
      else if accum < release_end {
         let rel = (accum - decay_end) / e.release;
         e.sustain - rel * e.sustain
      } else {
         0.0
      }
   }
}

/// Calls the generator to create a new source every time the trigger goes high.
/// The value of the trigger line is passed as a parameter to the generator.
pub fn triggered<Generator, L>(mut trigger: impl CV, mut generator: Generator) -> impl CV
   where
   L: CV,
   Generator: FnMut(Value) -> L + Clone
{
   let mut line = None;
   let mut ready = true;

   move |dt| {
      let trigger = trigger.sample(dt);
      if trigger > 0.0 {
         if ready {
            line = Some(generator(trigger));
         }
      } else {
         ready = true;
      }

      match &mut line {
         Some(l) => l.sample(dt),
         None => 0.0
      }
   }
}

pub fn reverb(mut l: impl CV) -> impl CV {
   use std::collections::VecDeque;

   let mut buf = VecDeque::new();
   let len = 113;

   #[derive(Clone)]
   struct Tap<T: FnMut(Value, Time) -> Value + Clone> {
      distance: usize,
      weight: f64,
      filter: T,
   }

   let mut taps = Vec::new();

   let make_filter = |filter_cutoff: f64| {
      let (mut filter_in, filter_line) = input_cv();
      let mut filter_out = lowpass(filter_line, filter_cutoff);
      move |value, dt| { filter_in(value); filter_out.sample(dt) }
   };

   taps.push(Tap { distance: 103, weight: 0.7, filter: make_filter(500.0) });
   taps.push(Tap { distance: 227, weight: 0.5, filter: make_filter(300.0) });
   taps.push(Tap { distance: 281, weight: 0.4, filter: make_filter(200.0) });

   let buf_size = 281 * len;
   let decay = 0.4;
   buf.resize(buf_size, 0.0);
   move |dt| {
      let input = l.sample(dt);
      let mut accum = 0.0;

      for tap in &mut taps {
         accum += (tap.filter)(buf[buf_size - tap.distance * len], dt) * tap.weight;
      }
      buf.pop_front();
      buf.push_back(input + accum * (1.0 - decay));
      input + accum
   }
}

pub fn lowpass(mut l: impl CV, mut cutoff: impl CV) -> impl CV {
   let mut prev = 0.0;
   move |dt| {
      let value = l.sample(dt);
      let f = cutoff.sample(dt);
      let b = 2.0 * std::f64::consts::PI * f * dt;
      let a = b / (b + 1.0);
      let res = a * value + (1.0 - a) * prev;
      prev = res;
      res
   }
}

pub fn input_cv() -> (impl FnMut(Value) + Clone, impl CV) {
   use std::sync::Arc;
   use std::cell::UnsafeCell;

   #[derive(Clone, Default)]
   struct SharedFloat(Arc<UnsafeCell<f64>>);
   unsafe impl Sync for SharedFloat {}
   unsafe impl Send for SharedFloat {}
   impl SharedFloat {
      fn set(&self, v: f64) { *unsafe { (&mut *self.0.get()) } = v }
      fn get(&self) -> f64 { *unsafe { (&*self.0.get()) } }
   }

   let v = SharedFloat::default();
   let _v = v.clone();
   ( move |new_v| _v.set(new_v), move |dt| v.get())
}

pub fn delay(mut l: impl CV, samples: usize) -> impl CV {
   use std::collections::VecDeque;
   let mut buffer = VecDeque::new();
   buffer.resize(samples, 0.0);
   move |dt| {
      buffer.push_back(l.sample(dt));
      buffer.pop_front().unwrap()
   }
}

pub fn white_noise() -> impl CV {
   let samples = 2048;
   let mut noise = Vec::new();
   for i in 0..samples { noise.push(rand::random::<f64>() * 2.0 - 1.0); }
   let mut i = 0;

   move |dt| {
      i = (i + 1) % samples;
      noise[i]
   }
}

pub fn string_length(freq: Hz, sample_rate: Hz) -> Value {
   // F = R / L
   sample_rate / freq
}

pub fn string(mut input: impl CV, length: f64, filter_cutoff: f64) -> impl CV {
   use std::collections::VecDeque;
   let mut i = 0;
   let length = length as usize;

   let mut left = VecDeque::new();

   let (mut filter_in, filter_line) = input_cv();
   let mut filter_out = lowpass(filter_line, filter_cutoff);
   let mut filter = move |value, dt| { filter_in(value); filter_out.sample(dt) };

   left.resize(length, 0.0);

   move |dt| {
      let input = input.sample(dt);
      let l = left.pop_front().unwrap();
      let v = 0.997 * filter(input + l, dt);
      left.push_back(v);
      v
   }
}

/// Creates an iterator over the partials of some fundamental frequency
pub fn harmonics<Oscillator, L>(oscillator: Oscillator, fundamental: Hz, n_harmonics: u32, inharmony: f64)
   -> impl Iterator<Item = L>
   where
   L: CV,
   Oscillator: Fn(Hz) -> L
{
   (1..=n_harmonics)
      .map(move |i| oscillator(fundamental * (i as f64).powf(1.0 + inharmony)))
}

pub fn linear_harmonics<Oscillator, L>(oscillator: Oscillator, fundamental: Hz, n_harmonics: u32, inharmony: f64)
   -> impl CV
   where L: CV, Oscillator: Fn(Hz) -> L
{
   mix::weighted_sum(
      harmonics(oscillator, fundamental, n_harmonics, inharmony).zip(
         (0..n_harmonics).map(|i| i + 1).map(|i| 1.0 / i as f64))
   )
}

#[test]
fn make_sound() {
   let ev_loop = EventLoop::new();
   let device = cpal::default_output_device()
      .expect("no output device available");

   let format = device.supported_output_formats()
      .expect("error querying output formats")
      .next()
      .expect("no supported output format")
      .with_max_sample_rate();

   let sample_rate = format.sample_rate.0;
   let dt = 1.0 / sample_rate as f64;
   let mut time = 0.0;
   println!("output format: {:?}, {:?}", format.data_type, format.sample_rate);

   let stream_id = ev_loop.build_output_stream(&device, &format).unwrap();
   let mut vol = 1.0;


   let base = 55; // semitone, 69 => A above middle C
   let chord = [0, 4, 7];
   let mut test = mix::sum(
      chord.iter().map({
         let mut j = 0;
         move |&i| {
            let freq = midi_to_freq((i + base) as f64);
            let len = string_length(freq, sample_rate as f64);
            let mut env = Envelope::adsr(0.001, 1.0 / freq, 1.0, 0.0);
            env.delay = j as f64 * 0.02;
            j += 1;
            let env = linear_envelope(env, 1.0);
            let pluck = mix::mul(white_noise(), env);
            let filter_cutoff = freq * 10.0;
            let pluck = string(pluck, len, filter_cutoff);
            let pluck = lowpass(pluck, freq);
            pluck
         }
      })
   );

   let mut test = reverb(test);

   ev_loop.play_stream(stream_id);
   let _ = std::panic::catch_unwind(std::panic::AssertUnwindSafe(|| {
      ev_loop.run(move |_stream_id, _stream_data| {
         match _stream_data {
            StreamData::Output { buffer: UnknownTypeOutputBuffer::I16(mut buffer) } => {
               let buf = &mut *buffer;
               let begin = time % 1.0;
               for v in buf {
                  time += dt;
                  let mut value = test.sample(dt);
                  if value > 1.0 { value = 1.0 }
                  if value < -1.0 { value = -1.0 }
                  *v = (std::i16::MAX as f64 * value * vol) as i16;
               }
            }
            _ => ()
         }
      });
   }));
}

#[test]
fn create_midi_input() {
   let input = MidiInput::new("synth").unwrap();

   let port_count = input.port_count();
   println!("port count: {}", port_count);

   for i in 0..port_count {
      println!("port {}: {}", i, input.port_name(i).unwrap());
   }
}
